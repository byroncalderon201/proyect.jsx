import './App.css'
import { BrowserRouter, Route, Routes } from 'react-router-dom'

//Lista de pestañas
import Home from './assets/Pages/Home'
import Cursos from './assets/Pages/Cursos'
import BarraNav  from './assets/components/BarraNav'
import Login from './assets/Pages/Login'
import Registro from './assets/Pages/Registro'
import Informacion from './assets/Pages/Informacion'
import Redes from './assets/Pages/Redes'
import Usuarios from './assets/Pages/Usuarios'
//----------------------------------------------------
function App() {
  return (
    <>
      <BrowserRouter>
        {/* Barra de navegación*/}
        <BarraNav/>
        <Routes>
          <Route path='/' element={<Home />}/>
          <Route path='/cursos' element={<Cursos />}/>
          <Route path='/login' element={<Login />}/>
          <Route path='/registro' element={<Registro />}/>
          <Route path='/redes' element={<Redes />}/>
          <Route path='/informacion' element={<Informacion />}/>
          <Route path='/usuarios' element={<Usuarios/>}/>
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
