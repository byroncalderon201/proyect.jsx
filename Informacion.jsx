import '../css/Informacion.css'

const Informacion = () => {
  return (
    <div className="tab-container">
      <div className="tab">
        <input type="radio" id="tab1" name="tab-group" defaultChecked />
        <label htmlFor="tab1">Información</label>
        <div className="content">
          <h2>¡Bienvenido/a a nuestra plataforma de cursos en línea!</h2>
          <p>Aquí en Uleam, nos emociona brindarte la oportunidad de aprender y crecer desde la comodidad de tu hogar. Nuestro objetivo es facilitar el acceso a conocimientos de calidad, impartidos por expertos en diferentes campos.</p>
          <p>Te invitamos a explorar nuestra plataforma y descubrir todo lo que tenemos para ofrecerte. Prepárate para adquirir nuevas habilidades, hacer conexiones significativas y llevar tu trayectoria al siguiente nivel.</p>
        </div>
      </div>
      <div className="tab">
        <input type="radio" id="tab2" name="tab-group" />
        <label htmlFor="tab2">Requisitos previos</label>
        <div className="content">
          <ul className='no-bullets'>
            <li>1. Tener conocimientos básicos sobre lógica de programación.</li>
            <li>2. Manejar un IDE o un editor de texto.</li>
            <li>3. Tiempo.</li>
          </ul>
        </div>
      </div>
      <div className="tab">
        <input type="radio" id="tab3" name="tab-group" />
        <label htmlFor="tab3">Instructores</label>
        <div className="content">
          <p>Nuestros instructores están calificados y certificados por la ULEAM.</p>
        </div>
      </div>
      
      <div className="social-media">
        <h2>Síguenos en redes sociales:</h2>
        <ul>
          <li><a href="https://www.facebook.com" target='blank_'>Facebook</a></li>
          <li><a href="https://www.twitter.com" target='blank_'>Twitter</a></li>
          <li><a href="https://www.instagram.com" target='blank_'>Instagram</a></li>
        </ul>
      </div>
    </div>
  );
};

export default Informacion
