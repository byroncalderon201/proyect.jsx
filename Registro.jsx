import '../css/registro.css';
import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';

function Register() {
  const [newUsername, setNewUsername] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [newEmail, setNewEmail] = useState('');

  const handleRegister = (event) => {
    event.preventDefault();

    // Verificar si el usuario ya está registrado en el almacenamiento local
    const storedUsername = localStorage.getItem('username');
    const storedEmail = localStorage.getItem('email');

    if (storedUsername === newUsername || storedEmail === newEmail) {
      alert('El usuario o el correo ya están registrados');
      return;
    }

    // Validar la longitud de la contraseña
    if (newPassword.length < 5) {
      alert('La contraseña debe tener al menos 5 caracteres');
      return;
    }

    // Almacenar el nuevo usuario, contraseña y correo en el almacenamiento local
    localStorage.setItem('username', newUsername);
    localStorage.setItem('password', newPassword);
    localStorage.setItem('email', newEmail);

    alert('Registro exitoso. Se iniciará sesión automáticamente.');

    // Limpiar los campos del formulario
    setNewUsername('');
    setNewPassword('');
    setNewEmail('');

    // Regresar al login
    window.location.href = '/cursos';
  };

  return (
    <div className='datos-contenedor'>
      <h1>Registrarse</h1>
      <form onSubmit={handleRegister}>
        <label htmlFor="newUsername">Nuevo Usuario:</label>
        <input
          type="text"
          id="newUsername"
          value={newUsername}
          onChange={(e) => setNewUsername(e.target.value)}
          required
        /><br/><br/>
        <label htmlFor="newEmail">Correo Electrónico:</label>
        <input
          type="email"
          id="newEmail"
          value={newEmail}
          onChange={(e) => setNewEmail(e.target.value)}
          required
        /><br/><br/>
        <label htmlFor="newPassword">Nueva Contraseña:</label>
        <input
          type="password"
          id="newPassword"
          value={newPassword}
          onChange={(e) => setNewPassword(e.target.value)}
          minLength="5" // Establecer la longitud mínima de la contraseña
          required
        /><br/><br/>
        <button type="submit">Registrarse</button>
        <NavLink to='/login'>
          <button type="submit">Regresar al inicio de sesión</button>
        </NavLink>
      </form>
    </div>
  );
}

export default Register;
