import '../css/Home.css'
const Home = () => {
  return (
    <div className="header">
      <div className="texto-principal">
        <h1 className="barra-principal_1">Capacitate con nosotros,<br></br> en nuestros cursos centrados en que tu aprendizaje tenga resultados; como crear un proyecto,<br></br> tu próximo portafolios, una aplicación, o ser más productivo en tus procesos de trabajo, todo esto totalmente a tu ritmo.</h1>
    </div>
    </div>
  )
}

export default Home