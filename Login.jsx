import '../css/login.css';
import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';

function Login() {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleLogin = (event) => {
    event.preventDefault();

    // Obtener los datos almacenados en el almacenamiento local
    const storedUsername = localStorage.getItem('username');
    const storedPassword = localStorage.getItem('password');

    if (username === storedUsername && password === storedPassword) {
      alert('Inicio de sesión exitoso, bienvenido');
      // Redirigir a la página principal después del inicio de sesión
      window.location.href = '/cursos';
    } else {
      alert('Usuario o contraseña incorrectos');
    }

    // Limpiar los campos del formulario
    setUsername('');
    setPassword('');
  };

  return (
    <div className='datos-contenedor'>
      <h1>Iniciar Sesión</h1>
      <form onSubmit={handleLogin}>
        <label htmlFor="username">Usuario:</label>
        <input
          type="text"
          id="username"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
          required
        /><br/>
        <label htmlFor="password">Contraseña:</label>
        <input
          type="password"
          id="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          required
        /><br/>
          <button type="submit">Iniciar Sesión</button>
          <NavLink to='/registro'>
            <button type="submit">Registrar</button>
          </NavLink>

      </form>
    </div>
  )
}

export default Login;




