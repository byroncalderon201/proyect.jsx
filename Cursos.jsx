/* eslint-disable jsx-a11y/anchor-is-valid */
import '../css/lista-cursos.css'
import '../scripts/lista-cursos'
import { NavLink } from 'react-router-dom'
import html1 from '../img/html1.png'
import html2 from '../img/html2.png'
import css1 from '../img/css1.png'
import css2 from '../img/css2.png'
import js1 from '../img/js1.png'
import js2 from '../img/js2.png'

const Cursos = () => {
  return (
    <main className="mid">
      <div className="wrap">
        <h1 className="h1">Lista de cursos</h1>
        <div className="store-wrapper">
          <div className="category-list">
            <a href="#" className="categoryItem" category="all">Todo</a>
            <a href="#" className="categoryItem" category="html">Html</a>
            <a href="#" className="categoryItem" category="css">Css</a>
            <a href="#" className="categoryItem" category="javascript">JavaScript</a>
          </div>
          <section className="product-list">
            <div className="product-item" category="html">
              <img src={html1} alt="imagen1" className="img" />
              <NavLink to="https://www.youtube.com/embed/kN1XP-Bef7w" target="_blank">HTML #1</NavLink>
            </div>
            <div className="product-item" category="html">
              <img src={html2} alt="imagen2" className="img" />
              <NavLink to="https://www.youtube.com/embed/MJkdaVFHrto" target="_blank">HTML #2</NavLink>
            </div>
            <div className="product-item" category="css">
              <img src={css1} alt="imagen3" className="img" />
              <NavLink to="https://www.youtube.com/embed/OWKXEJN67FE" target="_blank">CSS #1</NavLink>
            </div>
            <div className="product-item" category="css">
              <img src={css2} alt="imagen4" className="img" />
              <NavLink to="https://www.youtube.com/embed/6qko7Nbe8YA" target="_blank">CSS #2</NavLink>
            </div>
            <div className="product-item" category="javascript">
              <img src={js1} alt="imagen5" className="img" />
              <NavLink to="https://www.youtube.com/embed/z95mZVUcJ-E" target="_blank">JavaScript #1</NavLink>
            </div>
            <div className="product-item" category="javascript">
              <img src={js2} alt="imagen6" className="img" />
              <NavLink to="https://www.youtube.com/embed/ivdTnPl1ND0" target="_blank">JavaScript #2</NavLink>
            </div>
          </section>
        </div>
      </div>
    </main>
  );
};

export default Cursos;
